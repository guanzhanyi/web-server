#pragma once
/*
epoll的封装
*/

#include"timer.h"
#include<sys/epoll.h>
#include<unordered_map>
#include<algorithm>
#include<iostream>
#include<vector>
#include "channel.h"
#include "httpData.h"
#include "util.h"
class Epoll:noncopyable{
private:
    using ptr_channel=std::shared_ptr<Channel>;
public:
    Epoll();
    ~Epoll();
    void epoll_add(ptr_channel chan, int timeout);
    void epoll_mod(ptr_channel chan, int timeout);
    void epoll_del(ptr_channel chan);
    //调用epoll_wait()
    std::vector<ptr_channel> poll();
    std::vector<ptr_channel> geteventsreq(int event_num);
    void add_timer(ptr_channel chan,int timeout);
    int getepollfd();
    void handleexpired();
private:
    static const int M_MAXFDS = 100000;
    int m_epollfd;
    std::vector<epoll_event> m_events;
    std::shared_ptr<Channel> m_channels[M_MAXFDS];
    std::shared_ptr<HttpData> m_https[M_MAXFDS];
    TimerManager m_tq;
};