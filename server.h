/*

*/

#pragma once
#include <memory>
#include "channel.h"
#include "threadpool.h"
#include "eventloop.h"

class Server{
public:
    Server(EventLoop *base,int port, int nums=1);
    ~Server()=default;
    void start();
    void handleNewConn();
    void handThisConn();
private:
    bool m_started;
    EventLoop *m_base;
    int m_port;
    int m_listenfd;
    std::shared_ptr<Channel> m_accept_channel;
    std::unique_ptr<ThreadPool> m_threadpool;
    int m_threadNums;
    //系统最大并发连接数
    static const int MAXFDS= 100000;
};