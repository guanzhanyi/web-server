#pragma once
/*

*/

#include <string>
#include <memory>
#include <functional>
#include "eventloop.h"
#include "channel.h"
#include "log.h"

enum ConnectionState{
    H_CONNECTED=0,
    H_DISCONNECTING,
    H_DISCONNECTED
};

class TcpConnection{
public:
    TcpConnection(EventLoop *p, int fd);
    ~TcpConnection(){
        close(m_connfd);
    }
    EventLoop* getLoop();
    void handleClose();
    void set_event(uint32_t);
    void register_event();
    std::string& getInbuffer(){
        return m_inBuffer;
    }
    void setOutbuffer(const std::string &str){
        m_outBuffer=str;
    }
    void clearAndError(){
        record()<<"m_error change to true";
        m_error=true;
        m_inBuffer.clear();
        m_outBuffer.clear();
    }
    std::shared_ptr<Channel> getChannel(){
        return m_channel_ptr;
    }

public:
    void handleRead();
    void handleWrite();
    void handleError();
    void handleConn();
    void setCall(std::function<int()>&& func){
        m_callback=func;
    }
    void setSeperate(std::function<void()>&& func){
        m_seperate=func;
    }

private:
    EventLoop *m_loop;
    int m_connfd;
    std::shared_ptr<Channel> m_channel_ptr;
    std::string m_inBuffer;
    std::string m_outBuffer;
    ConnectionState m_conn_state;
    bool m_error;

    std::function<int()> m_callback;
    //?
    std::function<void()> m_seperate;
};