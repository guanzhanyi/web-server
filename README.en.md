# C++11/14 high performance web server
## Introduction
Lightweight web server, support get, post and other web request methods. Excellent performance, easy deployment, detailed code annotation
## Development deployment environment
-Operating system
```
uname -a
Linux iZ2zeje3d4ytsydegwg7jyZ 5.4.0-47-generic #51-Ubuntu SMP Fri Sep 4 19:50:52 UTC 2020 x86_ 64 x86_ 64 x86_ 64 GNU/Linux
```
- Compiler: clang version 10.0.0-4ubuntu1 GCC version 9.3.0 (Ubuntu 9.3.0-17ubuntu 1-20.04)
- Version control Git
- Editor vscode
- Pressure measuring tool
## Use
```
>make
>./main
```
## Core functions and technologies
- The priority queue of c++stl is used to realize the minimum heap timer and time-out connection is processed at a time
- Using epoll lt to monitor read and write events
- Using the master-slave reactor model
- A non blocking IO model using multithreading
## Questions
- The thread class doesn't understand how start is implemented, especially VEC_ The role of TID??
## Development plan
- Cache data with buffer, reduce the number of calls to read(), and write()
- Log library adds disable debug output and adds a thread to a separate tail log Library
- Add parameter options for main.cpp
- Specification code, including aesthetic code, unified use of private variable `m_` prefix
## Code implementation reference
- muduo