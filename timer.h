#pragma once
#include"httpData.h"
#include<queue>
#include<deque>
#include<memory>
#include"util.h"
#include"log.h"

class HttpData;

class TimerNode{
public:
    TimerNode(std::shared_ptr<HttpData> request,int timeout);
    ~TimerNode();
    TimerNode(const TimerNode& t);
    void update(int timeout);
    void clearReq();
    void setDeleted();
    bool isDeleted();
    bool isValid();
    uint32_t getexpired();
private:
    bool m_deleted;
    uint64_t m_expired;
    std::shared_ptr<HttpData> m_HttpData_ptr;

};

struct TimerNodeCmp{
    bool operator() (std::shared_ptr<TimerNode>& a,std::shared_ptr<TimerNode>& b){
        return a->getexpired()>b->getexpired();
    }
};

class TimerManager{
public:
    using timer_node_ptr=std::shared_ptr<TimerNode>;
    //增加Timer
    void add_timer(std::shared_ptr<HttpData> httpData, uint32_t timeout);
    //删除Timer
    void del_timer();
    //处理超时事件
    void handler_expired_event();
    const static uint32_t DEFAULT_TIMEROUT;
private:
    std::priority_queue<timer_node_ptr,std::deque<timer_node_ptr>,TimerNodeCmp> m_timer_queue;
    std::mutex m_mtx;
};