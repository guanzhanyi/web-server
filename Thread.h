#pragma once
#include <thread>
#include <mutex>
#include <condition_variable>
#include <functional>
#include "util.h"
//默认对线程采取的是detach策略，因为后台程序不会涉及线程安全问题
class Thread : noncopyable
{
public:
    using  ThreadFunc=std::function<void()>;
    bool m_started = false;  //start flag
    ThreadFunc m_func;   //thread function
    std::string m_name; //thrad name
    pid_t m_tid = -1;   //thread_id
    std::mutex t_m_mtx;
    std::condition_variable t_m_cv; //mutex与condition，总会用到吧
public:
    explicit Thread(const ThreadFunc&,const std::string& name = std::string());
    ~Thread() = default;
    void start();
    int tid() const;
    std::string name()const;
    void setDefaultName();
};