/*

*/
#include  <iostream>
#include  <getopt.h>
#include  <string>
#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>
#include  <fcntl.h>
#include  <sys/types.h>
#include  <unistd.h>
#include  <sys/wait.h>
#include  <sys/stat.h>
#include "log.h"
#include "server.h"
#include "eventloop.h"

int main(int argc, char* argv[]){
    int threadNum = 4;
    int port = 80;
    EventLoop mainloop;
    Server myserver(&mainloop,port,threadNum);
    myserver.start();
    mainloop.loop();
    return 0;
}
