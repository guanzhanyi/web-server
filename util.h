#pragma once
/*
基础库的封装
*/
#include<stdlib.h>
#include<string>

//noncopyable类
class noncopyable {
public:
    noncopyable(const noncopyable&) = delete;
    noncopyable& operator=(const noncopyable&) = delete;
protected:
    noncopyable()=default;
    ~noncopyable()=default;
};

void shutDownWR(int fd);
void handler_for_sigpipe();
int socket_bind_listen(int port);
int setNonBlocking(int fd);
void setNodelay(int fd);
ssize_t readn(int fd,void *buff, size_t n);
ssize_t readn(int fd,std::string &inBuffer, bool &zero);
ssize_t readn(int fd,std::string &inBuffer);
ssize_t writen(int fd, void *buff, size_t n);
ssize_t writen(int fd, std::string &sbuff);
