# C++11/14高性能web服务器

## 简介
轻量级web服务器, 支持GET、POST等web请求方法. 性能优秀 , 部署方便, 代码注释详细.
## 开发部署环境
- 操作系统
```
uname -a
Linux iZ2zeje3d4ytsydegwg7jyZ 5.4.0-47-generic #51-Ubuntu SMP Fri Sep 4 19:50:52 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
```
- 编译器:  clang version 10.0.0-4ubuntu1 gcc version 9.3.0 (Ubuntu 9.3.0-17ubuntu1~20.04)
- 版本控制 git
- 编辑器   vscode
- 压测工具 

## 使用
```
>make
>./main
```
## 核心功能和技术
- 使用C++STL的priority队列简单实现最小堆定时器, 定时处理超时连接
- 使用epoll的LT监听读写事件
- 使用主从Reactor模型
- 使用多线程的非阻塞IO模型

## 问题
- Thread类中不明白start是怎么实现的,特别是vec_tid的作用??

## 开发计划
- 用buffer缓存数据, 减少`read()`和`write()`调用次数
- 日志库增加禁用debug输出功能以及单独尾日志库增加一个线程
- 增加main.cpp的参数选项
- 规范代码,包括美观代码, private变量统一用`m_`前缀
## 代码实现参考
- muduo