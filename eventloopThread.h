/*
eventloop匹配的线程
*/

#pragma once
#include "eventloop.h"
#include "util.h"
#include "Thread.h"
#include <mutex>
#include <condition_variable>

class EventLoopThread:noncopyable{
private:
    //线程入口函数
    void threadFunc();
    EventLoop* m_loop;
    Thread m_thread;
    std::mutex m_mtx;
    std::condition_variable m_cv;
public:
    EventLoopThread();
    ~EventLoopThread();
    //开始loop
    //为什么不直接在线程启动时就返回eventloop呢?我觉得是因为线程启动有时延,如果返回了一个nullptr, 会使程序奔溃
    EventLoop *startLoop();
};