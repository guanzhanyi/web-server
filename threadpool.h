/*
 * @Description: 线程池的构建，本质就是一个个的eventloopthread 的集合
 */ 
#pragma once
#include <memory>
#include <vector>
#include "eventloop.h"
#include "eventloopThread.h"
#include "log.h"
#include "util.h"

using namespace std;

class ThreadPool : noncopyable
{
private:
    typedef shared_ptr<EventLoopThread> sp_evthread;
    EventLoop* baseloop_;
    bool started_;
    int numThreads_;
    int next_; //将来将以轮询的方式为每个线程分发fd
    vector<sp_evthread> threads_;
    vector<EventLoop*> loops_;
public:
    ThreadPool(EventLoop* base, int nums);
    ~ThreadPool(){record() <<"~ThreadPool()";}
    void start();
    EventLoop* getNextLoop();
};